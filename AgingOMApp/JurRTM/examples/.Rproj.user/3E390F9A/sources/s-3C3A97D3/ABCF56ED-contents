library("readxl")
library("tidyverse")
library("gridExtra")
library("ggpmisc")
library("AquaEnv")

source("/home/jurjen/Documents/BottomTrawl/scripts/remin_model_13C-DIC.R")

#=============================================================================
#  Read in data from Excel file
#=============================================================================
data_file <- "/home/jurjen/Documents/BottomTrawl/Data/EMB_238 data for Jurjen.xlsx"
data <- read_excel(data_file)
data_pw <- read_excel(data_file, 
                      sheet = "Porewater")

#use nicer column names for coding (no spaces/units/capitals)
#advantage is that excel file is not changed and retains info about the units
data <- rename(data, "core_id" = MUC, 
               "interval" = Interval, 
               "depth" = "Depth (cm)", 
               "watcont" = "Water Content", 
               "por" = "Porosity (for DBD 2.5)", 
               "toc" = "% TOC",
               "hg" = "THg (µg/kg)",
               "tn" = "% TN",
               "ts" = "% TS",
               "tic" = "% TIC")

data_pw <- rename(data_pw, "core_id" = MUC, 
                  "core_nr" = "Core #", 
                  "depth" = "Depth (cm)", 
                  "ca" = "Ca (mM)", 
                  "k" = "K (mM)", 
                  "mg" = "Mg (mM)",
                  "na" = "Na (mM)",
                  "po4" = "P (µM) PO4 ICP MS",
                  "si" = "Si (µM)",
                  "ba" = "Ba (µM)", 
                  "fe" = "Fe (µM)",
                  "li" = "Li (µM)",
                  "mn" = "Mn (µM)",
                  "sr" = "Sr (µM)",
                  "hs" = "Sulfide (µM)",
                  "nh4" = "NH4 (µM)",
                  "dic" = "DIC (mM)",
                  "so4" = "S (mM) mostly SO4",
                  "d13dic" = "13C-DIC",
                  "no2" = "NO2 (µM)",
                  "no3" = "NO3 (µM)")
data_pw <- select(data_pw, c("core_id", "depth", "nh4", "po4", 
                             "na", "k", "li", "sr", "ca", "dic",
                             "so4", "hs", "d13dic", "no2", "no3",
                             "mg"))
data_pw$nh4 <- data_pw$nh4*1e-3 #mol/m3
data_pw$po4 <- data_pw$po4*1e-3 #mol/m3
data_pw$sr <- data_pw$sr*1e-3 #mol/m3
data_pw$li <- data_pw$li*1e-3 #mol/m3
data_pw$hs <- data_pw$hs*1e-3 #mol/m3
data_pw$no2 <- data_pw$no2*1e-3 #mol/m3
data_pw$no3 <- data_pw$no3*1e-3 #mol/m3

#function to read out intervals in excel file
readInterval <- function(intvl, index = c(1,2)) 
  parse_number(unlist(strsplit(intvl,"-")))[index]

#mutate the interval, i.e. separate top and bottom in additional columns
data <-
  data %>% 
  mutate(slice_top = vapply(interval, readInterval, index=1, FUN.VALUE = 1)) %>%
  mutate(slice_bot = vapply(interval, readInterval, index=2, FUN.VALUE = 1))
#change the order of the columns and remove the old string interval column
data <- select(data, core_id, depth, slice_top, slice_bot, everything()) %>%
  select(-interval)

#=============================================================================
#  Salinity data and functions
#=============================================================================
salt_data <- select(data_pw, "core_id", "depth", "na", "k", "li", "sr", "ca")

convert_NatoS <- function(Na) {
  molCl_divby_molNa <- sw_comp("Cl") / molweight("Cl") / (sw_comp("Na") / molweight("Na"))
  Cl_est = Na * molCl_divby_molNa * 1e-3 * molweight("Cl")
  Cl_est * 1.80655
}

#sw_comp gives mass fractions, so that sum(sw_comp())=1
molNa_divby_molK <- sw_comp("Na") / molweight("Na") / (sw_comp("K") / molweight("K"))
molNa_divby_molSr <- sw_comp("Na") / molweight("Na") / (sw_comp("Sr") / molweight("Sr"))
molNa_divby_molCa <- sw_comp("Na") / molweight("Na") / (sw_comp("Ca") / molweight("Ca"))

salt_data %>% rowwise() %>%  mutate(sal_est = convert_NatoS(na)) -> salt_data
salt_data %>% mutate(k_scaled = k * molNa_divby_molK) -> salt_data
salt_data %>% mutate(sr_scaled = sr * molNa_divby_molSr) -> salt_data
salt_data %>% mutate(ca_scaled = ca * molNa_divby_molCa) -> salt_data

#=============================================================================
#  Auxiliary functions
#=============================================================================
plot_profile <- function(var, core) {
  plot_data <- NULL
  if (var %in% colnames(data))
    plot_data <- select(data, core_id, depth, var)
  if (var %in% colnames(data_pw))
    plot_data <- select(data_pw, core_id, depth, var)
  if (is.null(plot_data))
    stop("Variable or core is not listed in the data.")
  
  plot_data <- filter(plot_data, !is.na(.data[[var]]), core_id == core)
  
  if (nrow(plot_data) < 1)
    stop("There is no data to plot for the combinaiton of this variable and core.")


  ggplot(plot_data, aes_string(y = var, x = "depth")) +
    geom_point() +
    scale_x_reverse() +
    labs(y = var, x = "depth (cm)" ) +
    coord_flip()
}

#calculate sediment accumulation rate (SAR), accounting for compaction
calc_vinf <- function(age, depth, por_0, por_inf, alpha) {
  A <- (por_inf - por_0) / (1 - por_inf)
  v_inf <- (1/age) * (depth - A/alpha * (exp(-alpha * depth) -1) )
  return(unname(v_inf))
}

#=============================================================================
#  Automatic fitting of reaction-transport Mmdel
#=============================================================================

# caller to run a simulation
simulate <- function(parms) {
  print(formatC(signif(parms,digits=3), digits=3,format="fg", flag="#"))

  TO12C.in <- rep(0, length.out = N)
  NH4.in <- rep(0, length.out = N)
  PO4.in <- rep(0, length.out = N)
  DI12C.in <- rep(1e-3, length.out = N) #avoid division by 0
  DI13C.in <- rep(0, length.out = N)
  TO13C.in <- rep(0, length.out = N)
  TI12C.in <- rep(0, length.out = N)
  TI13C.in <- rep(0, length.out = N)
  Ca.in <- rep(Ca.ow, length.out = N)
  state <- c(TO12C.in, NH4.in, PO4.in, DI12C.in,  DI13C.in, TO13C.in, 
             TI12C.in, TI13C.in, Ca.in)
  
  chem.spec <- c("TO12C", "NH4", "PO4", "DI12C", "DI13C", "TO13C", 
                 "TI12C", "TI13C", "Ca")
  

  
  std <- steady.1D(y = state, func = model, parms = parms,
                   names = chem.spec, nspec = 9)
  return(std)
}

# function to score a fit
fitting_cost <- function(parms, meas_TOC, meas_NH4, meas_PO4, meas_DIC, meas_d13DIC, 
                         meas_TIC, meas_Ca, x_TOC, x_NH4, x_PO4, x_DIC, x_d13DIC, x_TIC, x_Ca) {
  
  parms <- c(parms)
  std <- simulate(parms)
  
  mod_TOC <- approx(x = grid$x.mid, (std$y[,"TO12C"] + std$y[,"TO13C"]) *1e2, xout = x_TOC*1e-2)$y
  mod_NH4 <- approx(x = grid$x.mid, std$y[,"NH4"], xout = x_NH4*1e-2)$y
  mod_PO4 <- approx(x = grid$x.mid, std$y[,"PO4"], xout = x_PO4*1e-2)$y
  mod_Ca <- approx(x = grid$x.mid, std$y[,"Ca"], xout = x_Ca*1e-2)$y
  
  norm_TOC_fact <- max(meas_TOC)
  norm_NH4_fact <- max(meas_NH4)
  norm_PO4_fact <- max(meas_PO4)
  norm_Ca_fact <- max(meas_Ca)
  
  error_TOC <- sum(((meas_TOC - mod_TOC) / norm_TOC_fact  )^2) / length(meas_TOC)
  error_NH4 <- sum(((meas_NH4 - mod_NH4) / norm_NH4_fact )^2) / length(meas_NH4)
  error_PO4 <- sum(((meas_PO4 - mod_PO4) / norm_PO4_fact )^2) / length(meas_PO4)
  error_Ca <- sum(((meas_Ca - mod_Ca) / norm_Ca_fact )^2) / length(meas_Ca)
  
  error_DIC <- 0
  if (!is.na(meas_DIC[1])) {
    mod_DIC <- approx(x = grid$x.mid, (std$y[,"DI12C"] + std$y[,"DI13C"]), xout = x_DIC*1e-2)$y
    norm_DIC_fact <- max(meas_DIC)
    error_DIC <- sum(((meas_DIC - mod_DIC) / norm_DIC_fact )^2) / length(meas_DIC) 
  }
  
  error_d13DIC <- 0
  if (!is.na(meas_d13DIC[1])) {
    mod_d13DIC <- approx(x = grid$x.mid, d13C(std$y[,"DI12C"], std$y[,"DI13C"]), xout = x_DIC*1e-2)$y
    norm_d13DIC_fact <- max(meas_d13DIC)
    error_d13DIC <- sum(((meas_d13DIC - mod_d13DIC) / norm_d13DIC_fact )^2) / length(meas_d13DIC) 
  }
  
  error_TIC <- 0
  if (!is.na(meas_TIC[1])) {
    mod_TIC <- approx(x = grid$x.mid, (std$y[,"TI12C"] + std$y[,"TI13C"]) *1e2, xout = x_TIC*1e-2)$y
    norm_TIC_fact <- max(meas_TIC)
    error_TIC <- sum(((meas_TIC - mod_TIC) / norm_TIC_fact  )^2) / length(meas_TIC)
  }
  
  return( error_TOC + error_NH4 + 0 * error_PO4 + error_DIC + error_d13DIC + error_TIC + 0.1 * error_Ca)
}

#main function to fit RTM parameters to core data
fit_data <- function(core) {
  #Set parameters that are core-specific
  if (core == "2_3") {
    toc_L <- 14
    
    Hg_max_depth <- 10 #cm
    por_L <- 15
    nc_ratio <<- 0.1213729
  }
  if (core == "5_2") {
    toc_L <- 10
  }
  if (core == "8_2") {
    toc_L <- 10
  }
  if (core == "10_2") {
    toc_L <- 7
    
    Hg_max_depth <- 4.5 #cm
    por_L <- 12
    nc_ratio <<- 0.1300168
  }
  if (core == "13_2") {
    toc_L <- 8

    Hg_max_depth <- 6 #cm
    por_L <- 12
    nc_ratio <<- 0.11#0.09075255
    
    TIC.swi <<- 0.05/100
  }
  if (core == "15_2") {
    toc_L <- 10
  }
  if (core == "17_3") {
    toc_L <- 7
  }
  if (core == "18_3") {
    toc_L <- 7
  }
  
  Hg_max_year <- 2021 - 1985 #number of years ago when there was the peak Hg input

  fit_depth <- 0.15
  toc_to_fit <- filter(data, core_id == core, depth*1e-2 < fit_depth &  !is.na(toc) ) %>%
    select(depth, toc)
  nh4_to_fit <- filter(data_pw, core_id == core, depth*1e-2 < fit_depth & !is.na(nh4)) %>%
    select(depth, nh4)
  po4_to_fit <- filter(data_pw, core_id == core, depth*1e-2 < fit_depth & !is.na(po4)) %>%
    select(depth, po4)
  dic_to_fit <- filter(data_pw, core_id == core, depth*1e-2 < fit_depth & !is.na(dic)) %>%
    select(depth, dic)
  d13dic_to_fit <- filter(data_pw, core_id == core, depth*1e-2 < fit_depth & !is.na("d13dic")) %>%
    select(depth, "d13dic")
  tic_to_fit <- filter(data, core_id == core, depth*1e-2 < fit_depth &  !is.na(tic) ) %>%
    select(depth, tic)
  ca_to_fit <- filter(data_pw, core_id == core, depth*1e-2 < fit_depth & !is.na(ca)) %>%
    select(depth, ca)
  
  if (dim(dic_to_fit)[1] < 1)
    dic_to_fit <- tibble(depth = NA, dic = NA)
  if (dim(d13dic_to_fit)[1] < 1 | sum(is.na(d13dic_to_fit[,2])) == nrow(d13dic_to_fit[,2])  )
    d13dic_to_fit <- tibble(depth = NA, "d13dic" = NA)

  #fit porosity
  if (core %in% c("2_3", "10_2", "13_2")) {
    por_func <- function(depth) {
      predict(fit, data.frame(depth=depth))
    }

    tabPor <- data %>%
      filter(core_id == core) %>%
      select(depth, por)

    fit <- nls(por ~ yf + (y0 - yf) * exp(-alpha * depth),
               data=filter(tabPor, depth < por_L) , start = list(y0 = 0.9, yf = 0.6, alpha = 0.1))
    fit_por_coefs <- coef(fit)

    por <<- setup.prop.1D(func = p.exp, y.0 = fit_por_coefs["y0"],
                          y.inf = fit_por_coefs["yf"], x.att = 1/(fit_por_coefs["alpha"]*1e2), grid = grid)

    tort_sq <<- 1 - 2 * log(por$int)
    por_inf <<- unname(fit_por_coefs["yf"])
  
    SAR <- calc_vinf(age = Hg_max_year, depth = Hg_max_depth*1e-2, 
                     por_0 = fit_por_coefs["y0"], por_inf = fit_por_coefs["yf"], 
                     alpha = fit_por_coefs["alpha"])
    
    
    # par(mfrow = c(1,2))
    # plot(y= tabPor$depth, x = tabPor$por, ylim = c(30, 0), xlim = c(0,1), pch = 19,
    #      xlab = "por", ylab = "depth (cmbsf)")
    # lines(x = por_func(seq(0, 30, length.out = 100)), y = seq(0, 30, length.out = 100), lwd =2  )
    # legend("bottomleft", legend = c("data", "fit"), lty = c(NA, 1), col = 1, pch = c(19, NA), lwd = 2 )
    # #lines(x = por$mid, y = grid$x.mid * 100, lty =2 , col = 2 )
    # u <- SAR * por_inf / por_func(seq(0, 30, length.out = 100))
    # w <- SAR * (1 - por_inf) / (1-por_func(seq(0, 30, length.out = 100)))
    # plot(x = u * 1e2, y = seq(0, 30, length.out = 100), ylim = c(30, 0),
    #   xlab = "burial vel. (cm/y)", ylab = "", type = "l",
    #   xlim = c(0, 0.32), lwd = 2)
    # lines(x = w*1e2, y = seq(0, 30, length.out = 100), col = 2, lwd = 2)
    # legend("bottomright", legend = c("solutes", "solids"), lty = 1, col = c(1,2), lwd = 2 )
  }
  
  
  # Estimate pH
  pH_data <- select(filter(data_pw, core_id == core), depth, ca, dic, na) %>%
    mutate(sal = convert_NatoS(na), #[g/kg]
           K_aragonite = aquaenv( S = sal, t = TC, P = P )$Ksp_aragonite * 1e6, #mmol^2/kg^2
           K_CO2 = aquaenv( S = sal, t = TC, P = P )$K_CO2 * 1e3, #mmol/kg
           K_HCO3 = aquaenv( S = sal, t = TC, P = P )$K_HCO3 * 1e3, #mmol/kg
           co3_eq = K_aragonite / ca,
           hplus_est = 0.5 * (-K_CO2 + sqrt( K_CO2^2 - 4 * (1 - dic/co3_eq) * K_CO2 * K_HCO3 )),
           pH_est = -log10(hplus_est*1e-3))
  
  #Impose pH on model grid
  fit_pH <-  nls(pH_est ~ yf + (y0 - yf) * exp(-alpha * depth),
                 data=pH_data, 
                 start = list(y0 = 8.2, yf = 7.2, alpha = 0.1))
  
  fit_pH_grid <- tibble(depth_cm = grid$x.mid * 1e2,
                        pH_fit = p.exp(x = grid$x.mid * 1e2,
                                       y.0 = coef(fit_pH)["y0"],
                                       y.inf = coef(fit_pH)["yf"],
                                       x.att = 1/coef(fit_pH)["alpha"]) )
  
  #Parameters to calculate mineral precipitation/dissolution reactions
  Ca.ow <<- mean(select(filter(data_pw, core_id == core, !is.na(ca)), ca)$ca) #[mM]
  K_aragonite <<- mean(pH_data$K_aragonite) # [mM]^2
  K_CO2 <<- mean(pH_data$K_CO2) #[mM]
  K_HCO3 <<- mean(pH_data$K_HCO3) #[mM]
  
  
  #length in cm 
  guess <- c(Db_max = 3e-3,
             Db_efold = 2e-2,
             k_max = 1e-1,
             k_efold = 1e-1,
             F_TOC = 27)
  
  lower_lim <- c(1e-11 * PerSecToPerYr, #Db_max [m2/yr]
                 1e-2, #Db_efold [m]
                 0, #k_max #[1/yr]
                 0, #k_efold [m]
                 0) #F_TOC
                 
                
  upper_lim <- c(1e-9 * PerSecToPerYr,  #Db_max [m2/yr]
                 0.1,  #Db_efold [m]
                 10, #k_max #[1/yr]
                 0.1, #k_efold [m]
                 1000) #F_TOC
                 
  
  v <<- SAR
  
  fit_obj <- nlminb(start = guess, objective = fitting_cost,
                    lower = lower_lim,
                    upper = upper_lim,
                    meas_TOC = toc_to_fit$toc,
                    meas_NH4 = nh4_to_fit$nh4,
                    meas_PO4 = po4_to_fit$po4,
                    meas_DIC = dic_to_fit$dic,
                    meas_d13DIC = d13dic_to_fit$d13dic,
                    meas_TIC = tic_to_fit$tic,
                    meas_Ca = ca_to_fit$ca,
                    x_TOC = toc_to_fit$depth,
                    x_NH4 = nh4_to_fit$depth,
                    x_PO4 = po4_to_fit$depth,
                    x_DIC = dic_to_fit$depth,
                    x_d13DIC = d13dic_to_fit$depth,
                    x_TIC = tic_to_fit$depth,
                    x_Ca = ca_to_fit$depth)

  par <- fit_obj$par
  
  std <- simulate( par )

  names(par) <- names(parms)

  print(formatC(signif(par,digits=3), digits=3,format="fg", flag="#"))
  
  toc_plot <-  ggplot(filter(data, core_id == core), 
          aes(y = toc, x = depth)) + 
    geom_point() +
    scale_x_reverse() +
    geom_line(data = data.frame(TOC = (std$y[,"TO12C"] + std$y[,"TO13C"]) *1e2, depth = grid$x.mid*1e2),
              aes(y = TOC, x = depth)) +
    labs(y = "TOC (wght %)") +
    coord_flip()
      
  nh4_plot <- ggplot(filter(data_pw, core_id == core),
         aes(y = nh4, x = depth)) +
    geom_point() +
    scale_x_reverse() +
    geom_line(data = data.frame(NH4 = std$y[,"NH4"], depth = grid$x.mid*1e2),
              aes(y = NH4, x = depth)) +
    labs(y = "NH4 (mM)") +
    coord_flip() +
    ylim(0, 0.7 )
  
  po4_plot <- ggplot(filter(data_pw, core_id == core),
         aes(y = po4, x = depth)) +
    geom_point() +
    scale_x_reverse() +
    geom_line(data = data.frame(PO4 = std$y[,"PO4"], depth = grid$x.mid*1e2),
              aes(y = PO4, x = depth)) +
    labs(y = "PO4 (mM)") +
    coord_flip()
  
  ca_plot <- ggplot(filter(data_pw, core_id == core),
                     aes(y = ca, x = depth)) +
    geom_point() +
    scale_x_reverse() +
    geom_line(data = data.frame(Ca = std$y[,"Ca"], depth = grid$x.mid*1e2),
              aes(y = Ca, x = depth)) +
    labs(y = "Ca (mM)") +
    coord_flip() +
    ylim(0, 10)
  
  dic_plot <- ggplot(filter(data_pw, core_id == core),
                     aes(y = dic, x = depth)) +
    geom_point() +
    scale_x_reverse() +
    geom_line(data = data.frame(DIC = (std$y[,"DI12C"] + std$y[,"DI13C"]), depth = grid$x.mid*1e2),
              aes(y = DIC, x = depth)) +
    labs(x = "DIC (mM)") +
    coord_flip()+
    ylim(0, 10)
  
  d13C_DIC_mod <- d13C(std$y[,"DI12C"], std$y[,"DI13C"])
  d13dic_plot <- ggplot(filter(data_pw, core_id == core),
                     aes(y = d13dic, x = depth)) +
    geom_point() +
    scale_x_reverse() +
    geom_line(data = data.frame(d13DIC2 = d13C_DIC_mod, depth = grid$x.mid*1e2),
              aes(y = d13DIC2, x = depth)) +
    labs(x = "d13C-DIC") +
    coord_flip() +
    ylim(-20, 0)
  
  tic_plot <-  ggplot(filter(data, core_id == core), 
                      aes(y = tic, x = depth)) + 
    geom_point() +
    scale_x_reverse() +
    geom_line(data = data.frame(TIC = (std$y[,"TI12C"] + std$y[,"TI13C"]) *1e2, depth = grid$x.mid*1e2),
              aes(y = TIC, x = depth)) +
    labs(y = "TIC (wght %)") +
    coord_flip()
  
  Db <- setup.prop.1D(func = p.exp, grid = grid, y.0 = par["Db_max"],
                      y.inf = 0, x.att = par["Db_efold"] )
  Db_plot <- ggplot(data.frame(Db = Db$mid/PerSecToPerYr, depth = grid$x.mid), 
                    aes(x = depth, y = Db)) +
    geom_line() +
    scale_x_reverse() +
    labs(y = "Db (m2/s)") +
    ylim(0, par["Db_max"]*1.1/PerSecToPerYr) +
    coord_flip()
  
  
  k <- setup.prop.1D(func = p.exp, grid = grid, y.0 = par["k_max"],
                     y.inf = 0, x.att = par["k_efold"] )
 
  k_plot <- ggplot(data.frame(k = k$mid, depth = grid$x.mid), 
                    aes(x = depth, y = k)) +
    geom_line() +
    scale_x_reverse() +
    labs(y = "k (1/yr)") +
    ylim(0, par["k_max"]*1.1) +
    coord_flip()
  
  grid.arrange(toc_plot, dic_plot, nh4_plot, po4_plot, ca_plot, 
               d13dic_plot, tic_plot, Db_plot, k_plot,
               nrow = 3, ncol = 3, top = core)
}




for (core_id in  unique(data_pw$core_id)[1]) {
  fit_data(core = core_id)
}