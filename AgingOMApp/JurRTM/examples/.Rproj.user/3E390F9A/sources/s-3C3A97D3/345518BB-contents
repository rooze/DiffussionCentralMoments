#script can be called by nummodel_data.R

#=============================================================================
# Steady state early diagenesis model OM mineralization
#=============================================================================
library(marelac)
library(ReacTran)
library("tidyverse")

#=============================================================================
# Units used in the program:
#=============================================================================

# Mass =  mol
# Space = m
# Time = yr

#=============================================================================
# Model domain and grid definition
#=============================================================================
L <- 80 * 1e-2    # depth of sediment domain [m]
N <- 500  # number of grid layers
grid <- setup.grid.1D(x.up = 0, L = L, N = N)

# A handy function for plotting profiles
plot_profile <- function(...) {
  plot(y = grid$x.mid, ylim = c(0.3,0), type = "l", 
       ylab = "depth (m)", ...)
}

#=============================================================================
# Model parameters:
#=============================================================================

# Environmental parameters
S <- 18 # salinity
TC <- 5 # temperature [deg C]
P <- 1.013 # pressure [bar]
PaToBar <- 1e-5 
PerSecToPerYr <- 365*24*60^2
dens_sed <- 2.6 * 1e6 #g/m3

por_inf <-  0.6466294

por <- setup.prop.1D(func = p.exp, y.0 = 0.8970393,
                     y.inf = 0.6466294 , x.att = 0.06568586, grid = grid)

tort_sq <- 1 - 2 * log(por$int)


# Diffusion coefficient uses routine 'diffcoeff' from 'marelac' package
Dmol.NH4  <- diffcoeff(S = S, t = TC, P = P, species = "NH4")$NH4 * PerSecToPerYr  # m2/s in m2/yr
Dmol.PO4 <- diffcoeff(S = S, t = TC, P = P, species = "PO4")$PO4 * PerSecToPerYr # m2/s in m2/yr
Dmol.DIC <- diffcoeff(S = S, t = TC, P = P, species = "HCO3")$HCO3 * PerSecToPerYr # m2/s in m2/yr
Dmol.Ca <- diffcoeff(S = S, t = TC, P = P, species = "Ca")$Ca * PerSecToPerYr # m2/s in m2/yr
Dmol.Fe <- diffcoeff(S = S, t = TC, P = P, species = "Fe")$Fe * PerSecToPerYr # m2/s in m2/yr
Dmol.Mn <- diffcoeff(S = S, t = TC, P = P, species = "Mn")$Mn * PerSecToPerYr # m2/s in m2/yr
Dmol.NO3 <- diffcoeff(S = S, t = TC, P = P, species = "NO3")$NO3 * PerSecToPerYr # m2/s in m2/yr
Dmol.O2 <- diffcoeff(S = S, t = TC, P = P, species = "O2")$O2 * PerSecToPerYr # m2/s in m2/yr
Dmol.HS <- diffcoeff(S = S, t = TC, P = P, species = "HS")$HS * PerSecToPerYr # m2/s in m2/yr
Dmol.SO4 <- diffcoeff(S = S, t = TC, P = P, species = "SO4")$SO4 * PerSecToPerYr # m2/s in m2/yr

v <- 0.000824262 #[m/yr]

NH4.ow <- 0
PO4.ow <- 0.5 * 10^-3 #[mM]
Ca.ow <- 6.7 #[mM]
Fe.ow <- 0
Mn.ow <- 0
NO3.ow <- 23 * 1e-3 #[mM]
O2.ow <- 225 * 1e-3 #[mM]
HS.ow <- 0
SO4.ow <- 15

#units: [g element / m2 / yr]
MW_Fe <- 55.847 #g/mol
MW_Mn <- 54.938 #g/mol
MW_PO4 <- 94.971 #g/mol
F_FeS <- 0
F_FeS2 <- 0
F_FeOxA <- 0.02 * MW_Fe #[g/m2/yr] this is 0.5 mol/m2/y ; 58 * 1e-3 mol m-2 y-1 Reed 2011; 2 mol/m2/y Rooze et al. 2016
F_FeOxB <- 0.02  * MW_Fe
F_MnOxA <- 0.0003 * MW_Mn #[g/m2/yr], 0.03 mmol m-2 yr-1 Reed 2011 #10 umol /cm2/yr (MSc verslag)
F_MnOxB <- 0.0003 * MW_Mn #[g/m2/yr], 0.03 mmol m-2 yr-1 Reed 2011 #10 umol /cm2/yr (MSc verslag)
F_P_Abs <- 0 * 0.1 * MW_PO4 #[g/m2/yr]  0.1 mol/m2/yr Rooze et al., 2016

#based on  2_3 (just to be able to run this script stand-alone)
fit_pH_grid <- tibble(depth_cm = grid$x.mid * 1e2,
                      pH_fit = p.exp(x = grid$x.mid * 1e2,
                                     y.0 = 7.94,
                                     y.inf = 7.43,
                                     x.att = 0.16) )

#Ca_const <- 6.7 #[mM] constant profile imposed over depth 
K_aragonite <- 3.6 * 1e-1#[mM]^2
K_CO2 <- 6.78 * 1e-4 #[mM]
K_HCO3 <- 3.0 * 1e-7 #[mM]

k_dis <- 0.1 #[y-1]
k_prec <- 1e4 #[mol/m3/y]

k6 <- 1 * 1e4 # [mM-1 y-1] 
k7 <- 2 * 1e4 # [mM-1 y-1] 
k8 <- 1.4 * 1e5 # [mM-1 y-1] 
k9 <- 160 # [mM-1 y-1] 
k10 <- 100*10 # [mM-1 y-1] 
k11 <- 300 # [mM-1 y-1] 
k12 <- 20 # [mM-1 y-1] Reed
k13 <- 8 # [mM-1 y-1] Reed
k14 <- 0.6 #[y-1]
k15 <- 2 #[mM-1 y-1]
k16 <- 1.8 #[yr-1]
k17 <- 1e-3 #[mM-1 y-1]
k18 <- 120 #[mM-1 y-1]
KmO2 <- 8 * 1e-3
KmNO3 <- 12 * 1e-3
KmMnOx <- 4 #[mmol/kg]
KmFeOx <- 65 #[mmol/kg]


# Calculate DIC concentration based on standard ocean water composition
# Cl.ow <- convert_StoCl(S) / molweight("Cl") * 1e3 #[mmol Cl / kg seawater]=[mM]
# DIC_species <- "HCO3" #c("HCO3", "CO3", "CO2")
# molCl_divby_molDIC <- sw_comp("Cl") / molweight("Cl") /
#   sum(sw_comp(DIC_species) / molweight(DIC_species))
# DIC.ow <- unname(Cl.ow / molCl_divby_molDIC) #[mM]
DIC.ow <- 1.9
TIC.swi <- 0.2 / 100 #weight perc.

#isotope parameters
d13C_TOC.flux <- -22 #permil VPDB (Meister et al., 2019)
d13C_DIC.ow <- -2 #permil VPDB (Meister et al., 2019)
d13C_TIC.flux <- -2
r_vpdb <- 0.0111802 #13C/12C ratio reference for VPDB (Meister et al., 2019)

r_13C_TOCflux <-  (d13C_TOC.flux * 1e-3 + 1) * r_vpdb #mol 13C/ mol 12C ratio of TOC flux at SWI
r_13C_TOCflux <- r_13C_TOCflux * 13 / 12 #gram 13C / gram 12C
f_13C_TOCflux <- r_13C_TOCflux / (1 + r_13C_TOCflux) #fraction 13C: gram 13C / (gram 12C + gram 13C)

r_13C_DICow <-  (d13C_DIC.ow * 1e-3 + 1) * r_vpdb #13C/12C ratio of TOC flux at SWI
f_13C_DICow <- r_13C_DICow / (1 + r_13C_DICow) #fraction 13C: 13C / (12C + 13C)

r_13C_TICflux <-  (d13C_TIC.flux * 1e-3 + 1) * r_vpdb #mol 13C/ mol 12C ratio of TIC flux at SWI
r_13C_TICflux <- r_13C_TICflux * 13 / 12 #gram 13C / gram 12C
f_13C_CarbSWI <- r_13C_TICflux / (r_13C_TICflux + 1)

DI12C.ow <- (1-f_13C_DICow) * DIC.ow
DI13C.ow <- f_13C_DICow * DIC.ow

d13C <- function(light, heavy ) {
  r <- heavy/light
  (r / r_vpdb - 1) * 1e3
}

d13C_grams <- function(light, heavy ) {
  d13C(light/12, heavy/13)
}

#ratios for remineralization reactions
nc_ratio <- 16/122
pc_ratio <- 1/106

alpha_remin <- 1

#ratio PO4 bound to iron oxides
PFeOxA_ratio <- 0.07
PFeOxB_ratio <- 0.0

# parms <- c(Db_max = 1e-10 * PerSecToPerYr, #[m2/yr]
#            Db_efold = 0.01, #[m-1]
#            k_max = 0.01, #[yr-1]
#            k_efold = 0.06, #[m-1]
#            F_TOC = 27, #[g C / m2 / yr]
#            TIC.swi = 0.5 * 0.3 / 100) #g C / g sed)
parms <- c(Db_max = 0.0142, #[m2/yr]
           Db_efold = 0.01, #[m-1]
           k_max = 4*6.66 * 1e-3, #[yr-1]
           k_efold = 0.0748, #[m-1]
           F_TOC = (1/2)*27.2) #[g C / m2 / yr]
           
           

#=============================================================================
# Model formulation
#=============================================================================


heaviside <- function(x, k = 50) {
  (1 + exp(-2*k*x))^-1
}

model <- function (t, state, parms) {
  with(as.list(parms), {
    
    # Initialization of state variables
    TO12C <- state[1:N] # #g C / g sed
    NH4 <- state[(N+1):(2*N)]
    PO4 <- state[(2*N+1):(3*N)]
    DI12C <- state[(3*N+1):(4*N)]
    DI13C <- state[(4*N+1):(5*N)]
    TO13C <- state[(5*N+1):(6*N)]
    TI12C <- state[(6*N+1):(7*N)]
    TI13C <- state[(7*N+1):(8*N)]
    Ca <- state[(8*N+1):(9*N)]
    Fe <- state[(9*N+1):(10*N)]
    Mn <- state[(10*N+1):(11*N)]
    NO3 <- state[(11*N+1):(12*N)]
    FeOxA <- state[(12*N+1):(13*N)]
    MnOxA <- state[(13*N+1):(14*N)]
    SO4 <- state[(14*N+1):(15*N)]
    O2 <- state[(15*N+1):(16*N)]
    FeS <- state[(16*N+1):(17*N)]
    HS <- state[(17*N+1):(18*N)]
    P_Abs <- state[(18*N+1):(19*N)]
    FeOxB <- state[(19*N+1):(20*N)]
    MnOxB <- state[(20*N+1):(21*N)]
    FeS2 <- state[(21*N+1):(22*N)]
    
    Db <- setup.prop.1D(func = p.exp, grid = grid, y.0 = Db_max,
                        y.inf = 0, x.att = Db_efold)
    k <- setup.prop.1D(func = p.exp, grid = grid, y.0 = k_max,
                       y.inf = 0, x.att = k_efold)
    
    #advective velocity for solute u(x) * phi(x) = phi(inf) * SAR
    #v = SAR, i.e. burial velocity below compaction zone
    u_vec <- v * por_inf / por$int #solutes
    w_vec <- v * (1-por_inf) / (1-por$int) #solids
    
    # Transport and reaction terms
    tranTO12C <- tran.1D(C = TO12C, flux.up = (1-f_13C_TOCflux) * F_TOC/dens_sed, D = Db$int, 
                         v = w_vec, VF = (1-por$int), dx = grid)
    tranTO13C <- tran.1D(C = TO13C, flux.up = f_13C_TOCflux * F_TOC/dens_sed, D = Db$int, 
                         v = w_vec, VF = (1-por$int), dx = grid)
    tranNH4 <- tran.1D(C = NH4, C.up = NH4.ow, D = Dmol.NH4/tort_sq + Db$int, 
                       v = u_vec, VF = por$int, dx = grid)
    tranPO4 <- tran.1D(C = PO4, C.up = PO4.ow, D = Dmol.PO4/tort_sq + Db$int, 
                       v = u_vec, VF = por$int, dx = grid)
    tranDI12C <- tran.1D(C = DI12C, C.up = DI12C.ow, D = Dmol.DIC/tort_sq + Db$int, 
                         v = u_vec, VF = por$int, dx = grid)
    tranDI13C <- tran.1D(C = DI13C, C.up = DI13C.ow, D = Dmol.DIC/tort_sq + Db$int, 
                         v = u_vec, VF = por$int, dx = grid)
    tranTI12C <- tran.1D(C = TI12C, C.up = TIC.swi * (1-f_13C_CarbSWI), D = Db$int, 
                         v = w_vec, VF = (1-por$int), dx = grid)
    tranTI13C <- tran.1D(C = TI13C, C.up = TIC.swi * f_13C_CarbSWI, D = Db$int, 
                         v = w_vec, VF = (1-por$int), dx = grid)
    tranCa <- tran.1D(C = Ca, C.up = Ca.ow, D = Dmol.Ca/tort_sq + Db$int, 
                       v = u_vec, VF = por$int, dx = grid)
    tranFe <- tran.1D(C = Fe, C.up = Fe.ow, D = Dmol.Fe/tort_sq + Db$int, 
                      v = u_vec, VF = por$int, dx = grid)
    tranMn <- tran.1D(C = Mn, C.up = Mn.ow, D = Dmol.Mn/tort_sq + Db$int, 
                      v = u_vec, VF = por$int, dx = grid)
    tranNO3 <- tran.1D(C = NO3, C.up = NO3.ow, D = Dmol.NO3/tort_sq + Db$int, 
                      v = u_vec, VF = por$int, dx = grid)
    tranFeOxA <- tran.1D(C = FeOxA, flux.up = F_FeOxA / dens_sed, D = Db$int, 
                      v = w_vec, VF = (1-por$int), dx = grid)
    tranFeOxB <- tran.1D(C = FeOxB, flux.up = F_FeOxB / dens_sed, D = Db$int, 
                         v = w_vec, VF = (1-por$int), dx = grid)
    tranMnOxA <- tran.1D(C = MnOxA, flux.up = F_MnOxA / dens_sed, D = Db$int, 
                        v = w_vec, VF = (1-por$int), dx = grid)
    tranMnOxB <- tran.1D(C = MnOxB, flux.up = F_MnOxB / dens_sed, D = Db$int, 
                         v = w_vec, VF = (1-por$int), dx = grid)
    tranSO4 <- tran.1D(C = SO4, C.up = SO4.ow, D = Dmol.SO4/tort_sq + Db$int, 
                      v = u_vec, VF = por$int, dx = grid)
    tranO2 <- tran.1D(C = O2, C.up = O2.ow, D = Dmol.O2/tort_sq + Db$int, 
                      v = u_vec, VF = por$int, dx = grid)
    tranFeS <- tran.1D(C = FeS, flux.up = F_FeS / dens_sed, D = Db$int, 
                        v = w_vec, VF = (1-por$int), dx = grid)
    tranHS <- tran.1D(C = HS, C.up = HS.ow, D = Dmol.HS/tort_sq + Db$int, 
                      v = u_vec, VF = por$int, dx = grid)
    tranP_Abs <- tran.1D(C = P_Abs, flux.up = F_P_Abs / dens_sed, D = Db$int, 
                        v = w_vec, VF = (1-por$int), dx = grid)
    tranFeS2 <- tran.1D(C = FeS2, flux.up = F_FeS2 / dens_sed, D = Db$int, 
                       v = w_vec, VF = (1-por$int), dx = grid)
    
    ################################################################################################
    ## Carbon reactions and associated isotope fractionation
    ################################################################################################
    
    #convert TOC % to mol TOC / m3 sediment
    TO12C_mM <- TO12C / 12 * dens_sed #[g C / g sed][molC / g C][g sed / m3 sed]=[mol C/m3 sed]
    TO13C_mM <- TO13C / 13 * dens_sed #[g C / g sed][molC / g C][g sed / m3 sed]=[mol C/m3 sed]
    TI12C_mM <- TI12C / 12 * dens_sed #[g C / g sed][molC / g C][g sed / m3 sed]=[mol C/m3 sed]
    TI13C_mM <- TI13C / 13 * dens_sed
    
    # reactions [mol/m3/yr]
    # mineralization in molC/m3(total volume)/yr
    R_12Cmin <- k$mid * TO12C_mM * (1 - por$mid)
    R_13Cmin <- alpha_remin * k$mid * TO13C_mM * (1 - por$mid)
    R_min <- R_12Cmin + R_13Cmin
    
    #Precipitation/Dissolution of carbonates
    r_DIC <- DI13C / DI12C
    f_DIC <- r_DIC / (r_DIC + 1)
    
    CO3 <- K_aragonite / Ca #[mM] 
    hplus <- 10^-(fit_pH_grid$pH_fit) * 1e3 #[mM] imposed, not constant
    DIC_eq <- CO3 * (hplus^2 + K_CO2 * hplus + K_CO2 * K_HCO3) / (K_CO2 * K_HCO3)
    
    CarbSat <- (DI12C + DI13C  - DIC_eq) / DIC_eq #positive: precipitation, negative: dissolution
    
    #reaction: positive = dissolution, negative = precipitation
    R_Carb12 <- (1-heaviside(CarbSat)) * (TI12C_mM > 0) * k_dis  * TI12C_mM * (1 - por$mid) - #dissolution
       heaviside(CarbSat) * (1-f_DIC) * k_prec  * CarbSat * por$mid #precipitation
    R_Carb13 <- (1-heaviside(CarbSat)) * (TI13C_mM > 0) * k_dis  * TI13C_mM * (1 - por$mid) - #dissolution
      heaviside(CarbSat) * f_DIC * k_prec  * CarbSat * por$mid #precipitation
    
    
    ################################################################################################
    ## Redox reactions
    ################################################################################################
    
    #convert metal % to mol metal / m3 sediment
    FeOxA_mM <- FeOxA / MW_Fe * dens_sed #[g Fe / g sed][molFe / g C][g sed / m3 sed]=[mol Fe/m3 sed]
    FeOxB_mM <- FeOxB / MW_Fe * dens_sed #[g Fe / g sed][molFe / g C][g sed / m3 sed]=[mol Fe/m3 sed]
    FeS_mM <- FeS / MW_Fe * dens_sed
    FeS2_mM <- FeS2 / MW_Fe * dens_sed
    MnOxA_mM <- MnOxA / MW_Mn * dens_sed #[g Mn / g sed][molMn / g C][g sed / m3 sed]=[mol Mn/m3 sed]
    MnOxB_mM <- MnOxB / MW_Mn * dens_sed #[g Mn / g sed][molMn / g C][g sed / m3 sed]=[mol Mn/m3 sed]
    
    ##### Primary redox reactions
    # OM + O2
    R1 <- R_min * O2 / (O2 + KmO2)
    # OM + NO3
    R2 <- R_min * KmO2 / (O2 + KmO2) * NO3 / (NO3 + KmNO3)
    # OM + MnOx
    R3 <- R_min * KmO2 / (O2 + KmO2) * KmNO3 / (NO3 + KmNO3) * MnOxA_mM / (MnOxA_mM + KmMnOx)
    # OM + FeOx
    R4 <- R_min * KmO2 / (O2 + KmO2) * KmNO3 / (NO3 + KmNO3) * KmMnOx / (MnOxA_mM + KmMnOx) * FeOxA_mM / (FeOxA_mM + KmFeOx)
    # OM + SO4
    R5 <- R_min * KmO2 / (O2 + KmO2) * KmNO3 / (NO3 + KmNO3) * KmMnOx / (MnOxA_mM + KmMnOx) * KmFeOx / (FeOxA_mM + KmFeOx)
    
    ##### Secondary redox reactions
    # NH4 + O2
    R6 <- (k6 * NH4 * O2) * por$mid
    # Mn + O2
    R7 <- (k7 * Mn * O2) * por$mid
    # Fe + O2
    R8 <- (k8 * Fe * O2) * por$mid
    # HS + O2
    R9 <- (k9 * HS * O2) * por$mid
    
    #Fe + HS
    R10 <- (k10 * Fe * HS) * por$mid
    
    #FeS + O2
    R11 <- (k11 * FeS_mM * O2) * (1 - por$mid)
    
    #FeOx + 1/8 HS + 1/2 H2O -> Fe + 1/8 SO4 + 3 OH- + 9/8 H+
    R12A <- (k12 * FeOxA_mM * HS) * (1 - por$mid)
    R12B <- (k12 * FeOxB_mM * HS) * (1 - por$mid)
    R12 <- R12A + R12B
    
    #MnO2 + 1/4 HS + H2O -> Mn + 1/4 SO4 +  + 2 OH- + 1/4 H+
    R13A <- (k13 * MnOxA_mM * HS) * (1 - por$mid)
    R13B <- (k13 * MnOxB_mM * HS) * (1 - por$mid)
    R13 <- R13A + R13B
    
    #FeOH3A -> FeOH3B
    R14 <- k14 * FeOxA_mM
    
    #MnO2 + Fe
    R15A <- k15 * MnOxA_mM * Fe
    R15B <- k15 * MnOxB_mM * Fe
    R15 <- R15A + R15B
    
    #MnOxA -> MnOxB
    R16 <- k15 * MnOxA_mM
    
    #FeS + 3/4 HS- + 1/4 SO4(2-) + 5/4 H+ -> FeS2 + H2O
    R17 <- k17 * FeS_mM * HS
    
    #FeS2 + O2
    R18 <- k18 * FeS2_mM * O2
    
    ################################################################################################
    ## Mass balances: Sum of reactions and transport
    ################################################################################################
    
    # rates in mol/m3_tot/yr)
    R.TO12C <-  (- R_12Cmin ) / (1-por$mid) * 12 / dens_sed
    R.TO13C <-  (- R_13Cmin ) / (1-por$mid) * 13 / dens_sed
    R.NH4 <- ( R_min * nc_ratio - R6 ) / por$mid
    R.PO4 <- ( R_min * pc_ratio + (4 * R4 - R8 + R12A - 2 * R15A) * PFeOxA_ratio +
                ( R12B - 2 * R15B) * PFeOxB_ratio - (PFeOxB_ratio - PFeOxA_ratio) * R14 ) / por$mid
    R.DI12C <- ( R_12Cmin + R_Carb12 ) / por$mid
    R.DI13C <- ( R_13Cmin + R_Carb13 ) / por$mid
    R.TI12C <- - R_Carb12  / (1-por$mid) * 12 / dens_sed
    R.TI13C <- - R_Carb13  / (1-por$mid) * 13 / dens_sed
    R.Ca <- (R_Carb12 + R_Carb13 ) / por$mid
    
    R.O2 <- (- R1 - 2 * R6 - 0.5 * R7 - 0.25 * R8 - 2 * R9 - 2 * R11 - 3.5 * R18) / por$mid
    R.NO3 <- (- 0.8 * R2 + R6) / por$mid
    R.MnOxA <- (-2 * R3 + R7 - R13A - R15A - R16) / (1 - por$mid) * MW_Mn / dens_sed
    R.MnOxB <- ( - R13B - R15B + R16) / (1 - por$mid) * MW_Mn / dens_sed
    R.FeOxA <- (-4 * R4 + R8 - R12A - R14 + 2 * R15) / (1 - por$mid) * MW_Fe / dens_sed
    R.FeOxB <- (- R12B + R14) / (1 - por$mid) * MW_Fe / dens_sed
    R.SO4 <- (- 0.5 * R5 + R9 + R11 + 0.125 * R12 + 0.25 * R13 - 0.25 * R17 + 2 * R18) / por$mid
    R.Mn <- (2 * R3 - R7 + R13 + R15) / por$mid
    R.Fe <- (4 * R4 - R8 - R10 + R11 + R12 - 2 * R15 + R18) / por$mid
    R.HS <- (0.5 * R5 - R9 - R10 - 0.125 * R12 - 0.25 * R13 - 0.75 * R17) / por$mid
    R.FeS <- (R10 - R11 - R17) / (1 - por$mid) * MW_Fe / dens_sed
    R.FeS2 <- (R17 - R18) / (1 - por$mid) * MW_Fe / dens_sed
    R.P_Abs <- (PFeOxA_ratio * (-4 * R4 + R8 - R12A + 2 * R15A) + 
                  PFeOxB_ratio * (- R12B + 2 * R15B) + (PFeOxB_ratio - PFeOxA_ratio) * R14  ) / (1 - por$mid) * MW_PO4 / dens_sed
    
    dTO12Cdt <- tranTO12C$dC + R.TO12C
    dTO13Cdt <- tranTO13C$dC + R.TO13C
    dNH4dt <- tranNH4$dC + R.NH4
    dPO4dt <- tranPO4$dC + R.PO4
    dDI12Cdt <- tranDI12C$dC + R.DI12C 
    dDI13Cdt <- tranDI13C$dC + R.DI13C
    dTI12Cdt <- tranTI12C$dC + R.TI12C
    dTI13Cdt <- tranTI13C$dC + R.TI13C
    dCadt <- tranCa$dC + R.Ca
    
    dO2dt <- tranO2$dC + R.O2
    dNO3dt <- tranNO3$dC + R.NO3
    dMnOxAdt <- tranMnOxA$dC + R.MnOxA
    dMnOxBdt <- tranMnOxB$dC + R.MnOxB
    dFeOxAdt <- tranFeOxA$dC + R.FeOxA
    dFeOxBdt <- tranFeOxB$dC + R.FeOxB
    dSO4dt <- tranSO4$dC + R.SO4
    dMndt <- tranMn$dC + R.Mn
    dFedt <- tranFe$dC + R.Fe
    dHSdt <- tranHS$dC + R.HS
    dFeSdt <- tranFeS$dC + R.FeS
    dFeS2dt <- tranFeS2$dC + R.FeS2
    dP_Absdt <- tranP_Abs$dC + R.P_Abs

    # Assemble the total rate of change, and return fluxes and reaction rates
    return(list(c(dTO12Cdt = dTO12Cdt, dNH4dt = dNH4dt, dPO4dt = dPO4dt, 
                  dDI12Cdt = dDI12Cdt, dDI13Cdt = dDI13Cdt, dTO13Cdt = dTO13Cdt,
                  dTI12Cdt = dTI12Cdt, dTI13Cdt = dTI13Cdt, dCadt = dCadt,
                  dFedt = dFedt, dMndt = dMndt, dNO3dt = dNO3dt, dFeOxAdt = dFeOxAdt,
                  dMnOxAdt = dMnOxAdt, dSO4dt = dSO4dt, dO2dt = dO2dt, dFeSdt = dFeSdt, 
                  dHSdt = dHSdt, dP_Absdt = dP_Absdt, dFeOxBdt = dFeOxBdt, dMnOxBdt = dMnOxBdt,
                  dFeS2dt = dFeS2dt), 
                F_NH4 = tranNH4$flux.up, F_PO4 = tranPO4$flux.up,
                F_DI12C = tranDI12C$flux.up, 
                F_DI13C = tranDI13C$flux.up,
                F_Ca = tranCa$flux.up, F_Ca = tranCa$flux.up,
                F_Mn = tranMn$flux.up, F_Fe = tranFe$flux.up,
                F_NO3 = tranNO3$flux.up, F_SO4 = tranSO4$flux.up,
                F_HS = tranHS$flux.up, F_O2 = tranO2$flux.up,
                F_R1 = sum(R1 * grid$dx), F_R2 = sum(R2 * grid$dx),
                F_R3 = sum(R3 * grid$dx), F_R4 = sum(R4 * grid$dx),
                F_R5 = sum(R5 * grid$dx), F_R6 = sum(R6 * grid$dx),
                F_R7 = sum(R7 * grid$dx), F_R8 = sum(R8 * grid$dx),
                F_R9 = sum(R9 * grid$dx), F_R10 = sum(R10 * grid$dx),
                F_R11 = sum(R11 * grid$dx), F_R12 = sum(R12 * grid$dx),
                F_R13 = sum(R13 * grid$dx), F_R14 = sum(R14 * grid$dx)
                ))
  })
}

#=============================================================================
# Steady state model solution
#=============================================================================

# Initialize state variables:
TO12C.in <- rep(0, length.out = N)
NH4.in <- rep(0, length.out = N)
PO4.in <- rep(0, length.out = N)
DI12C.in <- rep(1e-3, length.out = N) #avoid division by 0
DI13C.in <- rep(0, length.out = N)
TO13C.in <- rep(0, length.out = N)
TI12C.in <- rep(0, length.out = N)
TI13C.in <- rep(0, length.out = N)
Ca.in <- rep(Ca.ow, length.out = N) #avoid division by 0
Fe.in <- rep(0, length.out = N)
Mn.in <- rep(0, length.out = N)
NO3.in <- rep(0, length.out = N)
FeOxA.in <- rep(0, length.out = N)
MnOx.in <- rep(0, length.out = N)
SO4.in <- rep(0, length.out = N)
O2.in <- rep(0, length.out = N)
FeS.in <- rep(0, length.out = N)
HS.in <- rep(0, length.out = N)
P_Abs.in <- rep(0, length.out = N)
FeOxB.in <- rep(0, length.out = N)
MnOxB.in <- rep(0, length.out = N)
FeS2.in <- rep(0, length.out = N)

state <- c(TO12C.in, NH4.in, PO4.in, DI12C.in,  DI13C.in, TO13C.in, 
           TI12C.in, TI13C.in, Ca.in, Fe.in, Mn.in, NO3.in, FeOxA.in,
           MnOx.in, SO4.in, O2.in, FeS.in, HS.in, P_Abs.in, FeOxB.in, 
           MnOxB.in, FeS2.in)


# Steady state calculation of manganese profile
chem.spec <- c("TO12C", "NH4", "PO4", "DI12C", "DI13C", "TO13C", 
               "TI12C", "TI13C", "Ca", "Fe", "Mn", "NO3", "FeOxA",
               "MnOxA", "SO4", "O2", "FeS", "HS", "P_Abs", "FeOxB", 
               "MnOxB", "FeS2")

solid_names <- c("TO12C", "TO13C", "TI12C", "TI13C", "MnOxA", "FeOxA", 
                 "FeS", "P_Abs", "FeOxB", "MnOxB", "FeS2")
solute_names <- c("NH4", "PO4", "DI12C", "DI13C", "Ca", "Mn", "NO3", "SO4", "Fe", "HS", "O2")

std <- steady.1D(y = state, func = model, parms = parms,
                names = chem.spec, nspec = length(chem.spec), method = "runsteady")

steady.state.reached <- attributes(std)$steady
if (!steady.state.reached)
  stop("Model did not converge to steady state.")

#save steady state result in vector
state_std <- c(std$y) #convert matrix to vector


# #=============================================================================
# # Plot results
# #=============================================================================

plot_statevector <- function(state, name = "") {
  for (i_chem in 1:length(chem.spec)) {
    state_sv <- state[((i_chem-1)*N+1):(i_chem*N)]
    std$y[,i_chem] <- state_sv
  }

  par(mfrow = c(3,4))

  plot_profile(x = (std$y[,"TO12C"] + std$y[,"TO13C"]) * 100,
               xlab = paste("TOC", "wght (%)"), main = name)
  plot_profile(x = (std$y[,"DI12C"] + std$y[,"DI13C"]),
               xlab = paste("DIC", "(mol/m3)"), xlim = c(0,8))
  #lines(x = DIC_eq, y = grid$x.mid, col = "red", lty = 2)
  #points(y = pH_data$depth * 1e-2, x = pH_data$dic)
  plot_profile(x = (std$y[,"TI12C"] + std$y[,"TI13C"]) * 1e2,
               xlab = paste("TIC", "wght (%)"))
  plot_profile(x = std$y[,"NH4"],
               xlab = paste("NH4", "(mol/m3)"))
  plot_profile(x = d13C_grams(std$y[,"TO12C"], std$y[,"TO13C"]),
               xlab = paste("13C-TOC", "(mol/m3)"), xlim = c(-30, 0))
  plot_profile(x = d13C(std$y[,"DI12C"], std$y[,"DI13C"]),
               xlab = paste("13C-DIC", "(mol/m3)"), xlim = c(-30, 0))
  plot_profile(x = d13C_grams(std$y[,"TI12C"], std$y[,"TI13C"]),
               xlab = paste("13C-TIC", "(mol/m3)"))
  plot_profile(x = std$y[,"PO4"],
               xlab = paste("PO4", "(mol/m3)"))
  plot_profile(x = std$y[,"Ca"],
               xlab = paste("Ca", "(mol/m3)"))
  
  plot_profile(x = std$y[,"O2"],
               xlab = paste("O2", "(mol/m3)"))
  plot_profile(x = std$y[,"Fe"],
               xlab = paste("Fe", "(mol/m3)"))
  plot_profile(x = std$y[,"Mn"],
               xlab = paste("Mn", "(mol/m3)"))
  plot_profile(x = std$y[,"NO3"],
               xlab = paste("NO3", "(mol/m3)"))
  plot_profile(x = (std$y[,"MnOxA"] + std$y[,"MnOxB"]) / MW_Mn * 1e6,
               xlab = paste("MnOx", "(umol Mn / g)"))
  lines(x = std$y[,"MnOxA"]  / MW_Fe * 1e6,
        y = grid$x.mid, col = 2)
  lines(x = std$y[,"MnOxB"]  / MW_Fe * 1e6,
        y = grid$x.mid, col = 3, lty = 2)
  plot_profile(x = (std$y[,"FeOxA"] + std$y[,"FeOxB"])  / MW_Fe * 1e6,
               xlab = paste("FeOx", "(umol Fe / g)"), 
               xlim = c(0, max((std$y[,"FeOxA"] + std$y[,"FeOxB"])  / MW_Fe * 1e6)))
  lines(x = std$y[,"FeOxA"]  / MW_Fe * 1e6,
        y = grid$x.mid, col = 2)
  lines(x = std$y[,"FeOxB"]  / MW_Fe * 1e6,
        y = grid$x.mid, col = 3, lty = 2)
  plot_profile(x = std$y[,"SO4"],
               xlab = paste("SO4", "(mol/m3)"))
  plot_profile(x = std$y[,"HS"],
               xlab = paste("HS", "(mol/m3)"))
  plot_profile(x = std$y[,"FeS"] / MW_Fe * 1e6,
               xlab = paste("FeS", "(umol Fe / g)"))
  plot_profile(x = std$y[,"P_Abs"] / MW_PO4 * 1e6,
               xlab = paste("P-FeOx abs.", "(umol PO4 / g)"))
  plot_profile(x = std$y[,"FeS2"] / MW_Fe * 1e6,
               xlab = paste("FeS2", "(umol Fe / g)"))
  
  
}

plot_statevector(state_std)



