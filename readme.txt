Scripts corresponding to the applications in the paper are contained in this folder.

Application 1: PDE_and_Particles_sim.R
Application 2: ContinuousG_OrgMin.R
Application 3: AgingOMApp/run_simulation_tridiagonal.R
               AgingOMApp/run_simulation_transweibull.R

The first two scripts can be run after installing the packages that are listed in the first lines, i.e., deSolve and english for the PDE_and_Particles_sim.R and marelac, ReacTran, and rootSolve for ContinuousG_OrgMin.R.

The third application requires compilation of ./AgingOMApp/JurRTM/C/matrices_and_solvers.c. On linux/unix/mac, this is done in ./AgingOMApp/main.R that will be run automatically [see the line: system(paste("R CMD SHLIB ", pathC, "matrices_and_solvers.c", sep = ""))]. However, users using other operating systems may want to comment the line out and find another way to compile this c-code.The following packages must be installed to run the simulations: deSolve, english, nleqslv, numDeriv, FAdist.

All scripts were tested in R version 4.3.1 (2023-06-16).
